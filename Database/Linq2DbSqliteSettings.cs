﻿using System.Collections.Generic;
using System.Linq;
using LinqToDB.Configuration;

namespace Database
{
    public class Linq2DbSqliteSettings : ILinqToDBSettings
    {
        public IEnumerable<IDataProviderSettings> DataProviders => Enumerable.Empty<IDataProviderSettings>();
        public string? DefaultConfiguration => "SQLite";

        public string? DefaultDataProvider => "SQLite";
        private ConnectionStringSettings connectionStringSettings;

        public IEnumerable<IConnectionStringSettings> ConnectionStrings
        {
            get
            {
                yield return connectionStringSettings;
            }
        }

        public Linq2DbSqliteSettings(string connectionString)
        {
            connectionStringSettings = new ConnectionStringSettings()
            {
                ConnectionString = connectionString,
                Name = "server",
                ProviderName = "SQLite"
            };
        }

        private class ConnectionStringSettings : IConnectionStringSettings
        {
            public string ConnectionString { get; set; }
            public string Name { get; set; }
            public string? ProviderName { get; set; }
            public bool IsGlobal => false;
        }
    }
}