﻿using System;

namespace Database
{
    public class StoryDto
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string SubTitle { get; set; }

        public string StoryTextLink { get; set; }

        /// <summary>
        /// tag for the publication (ex BBC/ABC)
        /// </summary>
        public string Tag { get; set; }

        public DateTime PubDate { get; set; }
    }
}
