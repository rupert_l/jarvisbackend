﻿using System.Collections.Generic;
using System.Linq;
using LinqToDB.Configuration;
using LinqToDB.DataProvider.MySql;

namespace Database
{
    public class Linq2DbMySqlSettings : ILinqToDBSettings
    {
        public IEnumerable<IDataProviderSettings> DataProviders => Enumerable.Empty<IDataProviderSettings>();
        public string? DefaultConfiguration => "MySql";

        public string? DefaultDataProvider => "MySql";
        private ConnectionStringSettings connectionStringSettings;

        public IEnumerable<IConnectionStringSettings> ConnectionStrings
        {
            get
            {
                yield return connectionStringSettings;
            }
        }

        public Linq2DbMySqlSettings(string connectionString)
        {
            connectionStringSettings = new ConnectionStringSettings()
            {
                ConnectionString = connectionString,
                Name = "mysqlserver",
                ProviderName = "MySql"
            };
        }

        private class ConnectionStringSettings : IConnectionStringSettings
        {
            public string ConnectionString { get; set; }
            public string Name { get; set; }
            public string? ProviderName { get; set; }
            public bool IsGlobal => false;
        }
    }
}