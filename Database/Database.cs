﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataModels;
using LinqToDB;
using LinqToDB.Data;

namespace Database
{
    public static class Database
    {
        /// <summary>
        /// Deprecating
        /// </summary>
        /// <returns></returns>
        public static DateTime LastUpdateDepr()
        {
           using var db = new DbDB();
           return db.Metadatas.OrderBy(x => x.UpdateTime).Select(x=> x.UpdateTime).FirstOrDefault() ?? DateTime.MinValue;
        }

        public static DateTime LastUpdate(string tag)
        {
            using var db = new DbDB();
            return db.PublishQueues.OrderByDescending(x => x.PubDate).Select(x => x.PubDate).FirstOrDefault() ??
                   DateTime.MinValue;
        }

        public static void UpdateMetadata(DateTime latestUpdate, int no = 0)
        {
            using var db = new DbDB();
            db.Insert(new Metadata() {AddedStories = no, UpdateTime = latestUpdate});
        }

        /// <summary>
        /// retrieve all stories with optional date time
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<StoryDto> GetStories(DateTime? dt = null)
        {
            dt = dt ?? DateTime.Now;
            using var db = new DbDB();
            var s = db.PublishQueues.Where(x=>x.PubDate <= dt)
                .Select(x => new StoryDto()
            {
                Id = Guid.Parse(x.Id),
                PubDate = x.PubDate ?? DateTime.Now,
                StoryTextLink = x.Text,
                SubTitle = x.SubTitle,
                Tag = x.Tag,
                Title = x.Title
            });
            return s.ToList();
        }
        public static void InsertNewStories(IEnumerable<StoryDto> stories)
        {
            using var db = new DbDB();
            db.BulkCopy(stories.Select(x => new PublishQueue()
            {
                Id = x.Id.ToString(),
                PubDate = x.PubDate,
                SubTitle = x.SubTitle,
                Text = x.StoryTextLink,
                Title = x.Title
            }));
        }
    }
}
