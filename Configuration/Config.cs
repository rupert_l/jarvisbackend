﻿namespace Configuration
{
    public class Config
    {
        public string MySqlConnectionString { get; set; }

        public string SqliteConnectionString { get; set; }

        public string WebsiteUrl { get; set; }
        /// <summary>
        /// refers to the Wp_Term_Relationship entrance corresponding to the category created
        /// </summary>
        public ulong TermTaxonomyId { get; set; }
        public string AbcUrl { get; set; }
        public string BbcUrl { get; set; } 

        public string AbcMagicString { get; set; }

        public string BbcMagicString { get; set; }
    }
}
