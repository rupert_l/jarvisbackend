﻿# Jarvis Feed Quickstart

## Configuration 

- Replace the values in the config.demo.json with the production values
  - The feed values should be up to date, but will work with any feed from the same publisher
  - The Sqlite connection string is a path to the sqlite data file

## Running the Program
- Execution of the program without providing arguments will result in both the story scraper and the Wordpress publish queue update running in order. This may take some time to complete.
- Running with the `-localupdate` flag will run the story scraper only
- Running with the `-publishstories` flag will transmit the local stories in to the wordpress 
- Running with the `-test` flag will run tests on the various components
    - On a fresh install, the Sqlite test may return an error (expected non-zero). Running `localupdate` first will add data in for the program to test against.