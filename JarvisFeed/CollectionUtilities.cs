﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database;

namespace JarvisFeed
{
    public static class CollectionUtilities
    {
        public static DateTime ReturnLatestDate(IEnumerable<IEnumerable<StoryDto>> collections)
        {
            // todo need to rationalize so sources are seperate
            return collections.SelectMany(x => x).Max(x => x.PubDate);
        }
    }
}
