﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml;
using Database;
using Microsoft.SyndicationFeed;
using Microsoft.SyndicationFeed.Rss;

namespace JarvisFeed
{
    public class FeedReader : IUpdateResult
    {

        public string Url { get; set; }

        private RssFeedReader feedReader;

        public List<StoryDto> Result { get; private set; } 

        private IParser parser;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="url"></param>
        /// <param name="updateFrom">Pull stories from after this date</param>
        public FeedReader(string url, DateTime updateFrom, IParser parser)
        {
            var xml = XmlReader.Create(url, new XmlReaderSettings() {Async = true });
            this.parser = parser;
            feedReader = new RssFeedReader(xml);
        }

        public async void ReadArticles()
        {
            var RawContent = new List<StoryDto>();
            while (await feedReader.Read())
            {
                switch (feedReader.ElementType)
                {
                    case SyndicationElementType.Category:
                        ISyndicationCategory category = await feedReader.ReadCategory();
                        break;

                    // Read Image
                    case SyndicationElementType.Image:
                        ISyndicationImage image = await feedReader.ReadImage();
                        break;

                    // Read Item
                    case SyndicationElementType.Item:
                        ISyndicationItem item = await feedReader.ReadItem();
                        RawContent.Add(new StoryDto
                        {
                            Id = Guid.NewGuid(),
                            PubDate = item.Published.DateTime,
                            StoryTextLink = item.Links.FirstOrDefault()?.Uri.ToString() ?? "",
                            SubTitle = item.Description,
                            Title = item.Title,
                            Tag = parser.Tag
                        });
                        break;

                    // Read link
                    case SyndicationElementType.Link:
                        ISyndicationLink link = await feedReader.ReadLink();
                        break;

                    // Read Person
                    case SyndicationElementType.Person:
                        ISyndicationPerson person = await feedReader.ReadPerson();
                        break;

                    // Read content
                    default:
                        //pubdate
                        ISyndicationContent content = await feedReader.ReadContent();
                        break;
                }
            }

            var result = parser.TextContent(RawContent, DateTime.Today.AddDays(-2));

            Result = result.ToList();

        }

        public virtual void ParseArticles()
        {

        }

        public IEnumerable<StoryDto> ReturnedStories => Result;
    }
}
