﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using CsQuery;
using Database;

namespace JarvisFeed
{
    public class AbcParser : IParser
    {
        private string magic = Configuration.Configuration.Global.AbcMagicString; //"_1HzXw";
        public IEnumerable<StoryDto> TextContent(IEnumerable<StoryDto> htmlContent, DateTime dt)
        {
            var transform = htmlContent
                .Where(x=>x.PubDate > dt)
                .Select(Transform);
            return transform;
        }

        public string Tag => "ABC";

        public StoryDto Transform(StoryDto story)
        {
            var dom = CQ.CreateFromUrl(story.StoryTextLink);
            var result = dom[$".{magic}"];
            story.StoryTextLink = result.Text();

            return story;
        }
    }
}
