﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace JarvisFeed
{
    public static class WebGet
    {

        public static HttpContent GetContent(string url)
        {
            using HttpClient client = new HttpClient();

            using HttpResponseMessage response = client.GetAsync(url).Result;

            using HttpContent content = response.Content;

            return content;
        }

        public static string GetContentAsString(string url)
        {
            var content = GetContent(url);
            return content.ReadAsStringAsync().Result;
        }
    }
}
