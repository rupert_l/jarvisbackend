﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using Configuration;
using Database;
using LinqToDB.Data;
using LinqToDB.DataProvider.MySql;
using Newtonsoft.Json;
using Console = System.Console;
using Guid = System.Guid;

namespace JarvisFeed
{
    class Program
    {

        private static string Usage =>
            "Usage:\n\tNo Paramaters: Update the local cache then publish all unpublished stories." +
            "\n\t -localupdate: Update the local cache " +
            "\n\t -publishstories: Update the Wordpress publish queue from cache\n";

        private static Linq2DbMySqlSettings _mySqlSettings;

        static void Main(string[] args)
        {
            Configuration.Configuration.Global = Configuration.Configuration.ReadConfig();
            _mySqlSettings = new Linq2DbMySqlSettings(Configuration.Configuration.Global.MySqlConnectionString);
            DataConnection.AddConfiguration(_mySqlSettings.DefaultConfiguration,
                _mySqlSettings.ConnectionStrings.FirstOrDefault()?.ConnectionString ?? "",
                new MySqlDataProvider(_mySqlSettings.DefaultDataProvider));

            switch (args.Length)
            {
                // run both the scraper and wp update
                case 0:
                    UpdateStories();
                    WordpressUpdate();
                    break;
                case 1:
                    switch (args[0])
                    {
                        case "-localupdate":
                            UpdateStories();
                            break;
                        case "-publishstories":
                            WordpressUpdate();
                            break;
                        case "-test":
                            RunClientTests();
                            break;

                        default:
                            Console.WriteLine("Invalid argument\n" + Usage);
                            break;
                    }

                    break;
                default:
                    Console.WriteLine("Invalid number of arguments\n" + Usage);
                    break;

            }
        }


        public static void WordpressUpdate()
        {
            try
            {
                DataConnection.DefaultSettings =
                    new Linq2DbSqliteSettings(Configuration.Configuration.Global.SqliteConnectionString);

                var stories = Database.Database.GetStories();

                WordpressDb.AddPosts(stories.Select(x => new StoryDto()
                {
                    Id = Guid.NewGuid(),
                    PubDate = DateTime.Now,
                    StoryTextLink = x.StoryTextLink,
                    SubTitle = x.SubTitle,
                    Tag = x.Tag,
                    Title = x.Title
                }));
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occured that has prevented updating the Worpress publish Queue : \n\t" +
                                  ex.Message);
            }

        }

        public static void UpdateStories()
        {
            try
            {
                DataConnection.DefaultSettings =
                    new Linq2DbSqliteSettings(Configuration.Configuration.Global.SqliteConnectionString);

                var abcReader = new FeedReader(Configuration.Configuration.Global.AbcUrl,
                    Database.Database.LastUpdate("ABC"),
                    new AbcParser());

                var bbcReader = new FeedReader(Configuration.Configuration.Global.BbcUrl,
                    Database.Database.LastUpdate("BBC"),
                    new BbcParser());

                abcReader.ReadArticles();
                bbcReader.ReadArticles();
                Database.Database.InsertNewStories(abcReader.Result.Concat(bbcReader.Result));
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occured that has prevented updating the local story cache : \n\t" +
                                  ex.Message);
            }
        }






        public static void RunClientTests()
        {

            Console.WriteLine("Running Scraper Tests:\n");
            try
            {
                var abcReader = new FeedReader(Configuration.Configuration.Global.AbcUrl,
                    Database.Database.LastUpdate("ABC"),
                    new AbcParser());
                abcReader.ReadArticles();
                WriteOutError("ABC Parser", abcReader.Result.Count);
                var bbcReader = new FeedReader(Configuration.Configuration.Global.BbcUrl,
                    Database.Database.LastUpdate("BBC"),
                    new BbcParser());
                bbcReader.ReadArticles();
                WriteOutError("BBC Parser", bbcReader.Result.Count);
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred while running the parser tests :\n" + ex.Message);
            }

            try
            {
                Console.WriteLine("Running Sqlite Database Test:\n");
                DataConnection.DefaultSettings =
                    new Linq2DbSqliteSettings(Configuration.Configuration.Global.SqliteConnectionString);
                var cnt = Database.Database.GetStories();
                WriteOutError("Sqlite Database", cnt.Count());
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred while running Sqlite Test :\n" + ex.Message);
            }

            try
            {
                Console.WriteLine("Running MySql/Wordpress Database Test:\n");
                var firstOrDefault = WordpressDb.RetrievePosts().FirstOrDefault();
                Console.WriteLine("Connection Succeeded");
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred while running Mysql/Wordpress Database Test :\n" + ex.Message);
            }

        }

        private static void WriteOutError(string name, int count)
        {
            Console.WriteLine(
                $"{name} returned {count} stories. {(count == 0 ? "possible error (expected non-zero)" : string.Empty)}\n");
        }
    }

}
