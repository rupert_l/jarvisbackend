﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CsQuery;
using Database;

namespace JarvisFeed
{
    public class BbcParser : IParser
    {
        /// <summary>
        /// Leaving in for this comment:
        /// BBC seems to generate unique ids for the components directly wrapping story text,
        /// presumably to frustrate scrapers. A slightly more sophisticated approach is warranted
        /// as seen in the Transform function
        /// </summary>
        public string magic = Configuration.Configuration.Global.BbcMagicString;// "ssrcss-uf6wea-RichTextComponentWrapper e1xue1i83";
        public IEnumerable<StoryDto> TextContent(IEnumerable<StoryDto> htmlContent, DateTime dt)
        {
            var transform = htmlContent
                .Where(x => x.PubDate > dt)
                .Select(Transform);
            return transform;
        }

        public StoryDto Transform(StoryDto story)
        {
            var dom = CQ.CreateFromUrl(story.StoryTextLink);
            var result = dom.Select("div")
                .Where(x =>x.HasAttribute("data-component") 
                           && x.Attributes["data-component"] == "text-block");
            story.StoryTextLink = string.Concat(result.Select(x=> x.TextContent));

            return story;
        }

        public string Tag => "BBC";

    }
}
