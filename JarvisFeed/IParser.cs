﻿using System;
using System.Collections.Generic;
using System.Text;
using Database;

namespace JarvisFeed
{
    public interface IParser
    {
        public IEnumerable<StoryDto> TextContent(IEnumerable<StoryDto> HtmlContent, DateTime cutoff);

        public string Tag { get; }
    }
}
